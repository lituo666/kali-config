#!/bin/sh

echo "======================================================================================="
echo "Backup /etc/apt/sources.list"
sudo cp /etc/apt/sources.list /etc/apt/sources.list.bak

echo "======================================================================================="
echo "Modify source address"
sudo bash -c "cat > /etc/apt/sources.list" << EOF
deb https://mirror.tuna.tsinghua.edu.cn/kali kali-rolling main contrib non-free
#deb-src https://mirror.tuna.tsinghua.edu.cn/kali kali-rolling main contrib non-free
EOF

echo "======================================================================================="
echo "Update source"
sudo apt-get update

echo "======================================================================================="
echo "Install common software"
sudo apt-get install -y vim zsh git ibus ibus-pinyin

echo "======================================================================================="
echo "Change the time zone to Shanghai"
sudo timedatectl set-timezone "Asia/Shanghai"

echo "======================================================================================="
echo "Set Chinese"
echo "select zh_CN.UTF-8 UTF-8"
echo "select zh_CN.UTF-8"
echo "Wait 10 seconds"
sleep 10s
sudo dpkg-reconfigure locales

echo "======================================================================================="
echo "Restart after 30 seconds, otherwise press ctrl+z to stop"
sleep 30s
sudo reboot