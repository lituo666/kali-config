#!/bin/sh

echo "======================================================================================="
echo "Temporarily configure the network"
echo "Ip Address:    192.168.1.51/24"
echo "Gateway:       192.168.1.2"
echo "DNS Server:    192.168.1.2"

sudo ifconfig eth0 192.168.1.51/24
sudo route add default gw 192.168.1.2
sudo bash -c 'echo "nameserver 192.168.1.2" > /etc/resolv.conf'

echo "Configuration complete"
echo "======================================================================================="
