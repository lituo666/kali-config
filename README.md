# Kali 使用记录

## SSH
树莓派4B Kali系统默认开启SSH，用户名和密码都是`kali`

## 更换源
``` bash
sudo vim /etc/apt/sources.list
```
修改为
``` bash
deb https://mirror.tuna.tsinghua.edu.cn/kali kali-rolling main contrib non-free
#deb-src https://mirror.tuna.tsinghua.edu.cn/kali kali-rolling main contrib non-free
```
然后更新
``` bash
sudo apt-get update
```
更新失败可能是时间不对，需要[更改时区](#更改时区)

## 修改用户密码

```bash
sudo -i				#进入管理员
passwd kali			# kali 需要修改密码的用户
```
<span id="更改时区"></span>

## 更改时区

```bash
sudo timedatectl set-timezone "Asia/Shanghai"    #修改时区为上海
```

## 设置中文

``` bash
sudo dpkg-reconfigure locales
```
空格键选中`zh_CN.UTF-8 UTF-8`，回车键确认

![设置1](./image/设置中文1.png)

![设置2](./image/设置中文2.png)

![设置3](./image/设置中文3.png)

重启设备，登录桌面环境，选择保留旧的名称

![设置4](./image/设置中文4.png)

## 安装中文输入法
``` bash
sudo apt-get install ibus ibus-pinyin
```
安装完成后重启设备，`Win+空格`切换输入法

## 安装Gnome桌面环境

参考 [安装KDE桌面](https://linuxconfig.org/how-to-install-kde-dekstop-on-kali-linux)、[安装Gnome桌面](https://linuxconfig.org/how-to-install-gnome-desktop-on-kali-linux)
```bash
sudo apt update
sudo apt install kali-desktop-gnome
```

方向键上下选择`gdm3`，然后回车，等待大约20分钟安装完成，然后重启，等待显示桌面。

## 临时配置网络

配置临时地址为`192.168.1.51`

```bash
sudo ifconfig eth0 192.168.1.51/24
```

配置临时网关为`192.168.1.2`

```bash
sudo route add default gw 192.168.1.2
```

配置DNS服务器为`192.168.1.2`

```bash
sudo bash -c 'echo "nameserver 192.168.1.2" > /etc/resolv.conf'
```

也可保存下面的脚本，需要时运行即可

```bash
#!/bin/sh

echo "======================================================================================="
echo "Temporarily configure the network"
echo "Ip Address:    192.168.1.51/24"
echo "Gateway:       192.168.1.2"
echo "DNS Server:    192.168.1.2"

sudo ifconfig eth0 192.168.1.51/24
sudo route add default gw 192.168.1.2
sudo bash -c 'echo "nameserver 192.168.1.2" > /etc/resolv.conf'

echo "Configuration complete"
echo "======================================================================================="
```

## 添加字体

添加微软字体，以防打开windows文件时乱码，安装时尽量挂代理。

```bash
sudo apt-get install ttf-mscorefonts-installer
```

## 安装ohmyzsh

需要科学上网

```bash
sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"
```

安装完成后，打开`.zshrc`将主题改为`amuse`

-----
使用脚本进行基本配置配置

```bash
#!/bin/sh

echo "======================================================================================="
echo "Backup /etc/apt/sources.list"
sudo cp /etc/apt/sources.list /etc/apt/sources.list.bak

echo "======================================================================================="
echo "Modify source address"
sudo bash -c "cat > /etc/apt/sources.list" << EOF
deb https://mirror.tuna.tsinghua.edu.cn/kali kali-rolling main contrib non-free
#deb-src https://mirror.tuna.tsinghua.edu.cn/kali kali-rolling main contrib non-free
EOF

echo "======================================================================================="
echo "Update source"
sudo apt-get update

echo "======================================================================================="
echo "Install common software"
sudo apt-get install -y vim zsh git ibus ibus-pinyin

echo "======================================================================================="
echo "Change the time zone to Shanghai"
sudo timedatectl set-timezone "Asia/Shanghai"

echo "======================================================================================="
echo "Set Chinese"
echo "select zh_CN.UTF-8 UTF-8"
echo "select zh_CN.UTF-8"
echo "Wait 10 seconds"
sleep 10s
sudo dpkg-reconfigure locales

echo "======================================================================================="
echo "Restart after 30 seconds, otherwise press ctrl+z to stop"
sleep 30s
sudo reboot
```
